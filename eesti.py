import json
import urllib.request
from typing import Dict, Iterable, List, Type
import logging

import pandas
from estnltk.vabamorf.morf import synthesize
from estnltk import Text
from estnltk.taggers import VabamorfTagger
import streamlit as st


cases = [
    # label, Estonian case name, English case name
    ("n", "nimetav", "nominative"),
    ("g", "omastav", "genitive"),
    ("p", "osastav", "partitive"),
]

# https://github.com/estnltk/estnltk/blob/4236f2033110d2bf20fc7f565950c0a2170f8573/tutorials/nlp_pipeline/B_morphology/00_tables_of_morphological_categories.ipynb
parts_of_speech = {
    "A": "Adjective",
    "C": "Adjective",
    "D": "Adverb",
    "V": "Verb",
    "S": "Noun",
}


anki_url = "http://127.0.0.1:8765"


def request(action, **params):
    return {"action": action, "params": params, "version": 6}


def invoke(action, **params):
    requestJson = json.dumps(request(action, **params)).encode("utf-8")
    response = json.load(
        urllib.request.urlopen(urllib.request.Request(anki_url, requestJson))
    )
    if len(response) != 2:
        raise Exception("response has an unexpected number of fields")
    if "error" not in response:
        raise Exception("response is missing required error field")
    if "result" not in response:
        raise Exception("response is missing required result field")
    if response["error"] is not None:
        raise Exception(response["error"])
    return response["result"]


def create_card(deck, foreground, background, tags):
    note = {
        "deckName": deck,
        "modelName": "Basic",
        "fields": {
            "Front": foreground,
            "Back": background,
        },
        "tags": tags,
    }
    result = invoke(
        "addNote",
        note=note,
    )
    st.write("Card created")


def submitted():
    st.session_state.submitted = True


def reset():
    st.session_state.submitted = False


def all_equal(iterator):
    iterator = iter(iterator)
    try:
        first = next(iterator)
    except StopIteration:
        return True
    return all(first == x for x in iterator)


def default_card(word, ukrainian):
    st.session_state.word_foreground = f'Як по-естонськи буде "{ukrainian}"?'
    st.session_state.word_background = word
    st.checkbox("Create default card", key="default", value=True)
    st.table(
        pandas.DataFrame(
            {
                "foreground": [st.session_state.word_foreground],
                "background": [st.session_state.word_background],
            },
            columns=["foreground", "background"],
        )
    )


def noun_adj_card(word, pos, ukrainian):
    case_rows = [c for (_, c, _) in cases]

    singulars = []
    for case, case_name_est, case_name_eng in cases:
        singulars.append(synthesize(word, "sg " + case, pos))
    st.session_state.singular_foreground = (
        f'Як по-естонськи буде "{ukrainian}" в Nim-Oma-Osa?'
    )
    background_items = ["/".join(s) for s in singulars]
    if len(background_items) > 1 and all_equal(background_items):
        st.session_state.singular_background = f"{background_items[0]} (x3)"
    else:
        st.session_state.singular_background = " - ".join(background_items)
    st.checkbox("Create singular card", key="singular", value=True)
    st.table(
        pandas.DataFrame(
            {
                "foreground": [st.session_state.singular_foreground],
                "background": [st.session_state.singular_background],
            },
            columns=["foreground", "background"],
        )
    )
    # st.table(
    #    pandas.DataFrame(
    #        {"case": case_rows, "singular": [",".join(s) for s in singulars]},
    #        columns=["case", "singular"],
    #    )
    # )

    plurals = []
    for case, case_name_est, case_name_eng in cases:
        plurals.append(synthesize(word, "pl " + case, pos))
    st.session_state.plural_foreground = (
        f'Як по-естонськи буде множина "{ukrainian}" в Nim-Oma-Osa?'
    )
    background_items = ["/".join(s) for s in plurals]
    if len(background_items) > 1 and all_equal(background_items):
        st.session_state.plural_background = f"{background_items[0]} (x3)"
    else:
        st.session_state.plural_background = " - ".join(background_items)
    st.checkbox("Create plural card", key="plural")
    st.table(
        pandas.DataFrame(
            {
                "foreground": [st.session_state.plural_foreground],
                "background": [st.session_state.plural_background],
            },
            columns=["foreground", "background"],
        )
    )
    # st.table(
    #    pandas.DataFrame(
    #        {"case": case_rows, "plural": [",".join(s) for s in plurals]},
    #        columns=["case", "plural"],
    #    )
    # )


def verb_card(word, ukrainian):
    verbs = []
    for v in ["ma", "da"]:
        verbs.append(synthesize(word, v, "V"))
    st.session_state.verb_foreground = (
        f'Як по-естонськи буде -ma/-da інфінітив "{ukrainian}"?'
    )
    st.session_state.verb_background = (
        f"-ma: {','.join(verbs[0])}<br />-da: {','.join(verbs[1])}"
    )
    st.checkbox("Create verb card", key="verb", value=True)
    st.table(
        pandas.DataFrame(
            {
                "foreground": [st.session_state.verb_foreground],
                "background": [st.session_state.verb_background],
            },
            columns=["foreground", "background"],
        )
    )


# Initialize new morphological analyser
morph_tagger = VabamorfTagger()

word = st.text_input("Word", placeholder="Sõna...", key="word")

ukrainian = st.text_input("Слово", placeholder="Переклад...", key="ukrainian")

if st.button("Analyze", on_click=reset):
    with st.form("create_card"):
        # Prepare text
        text = Text(word)
        # Tag layers required by morph analysis
        text.tag_layer(["words", "sentences"])

        # Tag morph analysis
        morph_tagger.tag(text)

        options = []
        for w in text.morph_analysis:
            for pos in w.partofspeech:
                options.append(
                    {
                        "text": w.text,
                        "partofspeech": parts_of_speech.get(pos, "Other"),
                        "pos": pos,
                    }
                )
        selects = [f"{o['text']} ({o['partofspeech']})" for o in options]
        word_version = st.selectbox(
            "Part of speech",
            selects,
            placeholder="Option...",
            key="part_of_speech",
        )

        decks = invoke("deckNames")
        deck = st.selectbox(
            "Anki Deck",
            decks,
            index=None,
            placeholder="Deck...",
            key="anki_deck",
        )

        pos = options[selects.index(word_version)]["partofspeech"]
        if pos in ["Adjective", "Noun"]:
            st.session_state.tag = pos.lower()
            noun_adj_card(word, options[selects.index(word_version)]["pos"], ukrainian)
            st.form_submit_button("Create", type="primary", on_click=submitted)
        elif pos in ["Verb"]:
            st.session_state.tag = pos.lower()
            verb_card(word, ukrainian)
            st.form_submit_button("Create verb", type="primary", on_click=submitted)
        else:
            st.session_state.tag = pos.lower()
            default_card(word, ukrainian)
            st.form_submit_button("Create card", type="primary", on_click=submitted)

if "submitted" in st.session_state:
    if st.session_state.submitted == True:
        if "singular" in st.session_state and st.session_state.singular == True:
            create_card(
                st.session_state.anki_deck,
                st.session_state.singular_foreground,
                st.session_state.singular_background,
                [
                    "automatic",
                    "omastav",
                    "osastav",
                    "singular",
                    "translation",
                    st.session_state.tag,
                ],
            )
        else:
            st.write("Not creating a singular card")

        if "plural" in st.session_state and st.session_state.plural == True:
            create_card(
                st.session_state.anki_deck,
                st.session_state.plural_foreground,
                st.session_state.plural_background,
                [
                    "automatic",
                    "omastav",
                    "osastav",
                    "plural",
                    "translation",
                    st.session_state.tag,
                ],
            )
        else:
            st.write("Not creating a plural card")

        if "verb" in st.session_state and st.session_state.verb == True:
            create_card(
                st.session_state.anki_deck,
                st.session_state.verb_foreground,
                st.session_state.verb_background,
                ["automatic", "verb", "translation"],
            )
        else:
            st.write("Not creating a verb card")

        if "default" in st.session_state and st.session_state.default == True:
            create_card(
                st.session_state.anki_deck,
                st.session_state.word_foreground,
                st.session_state.word_background,
                ["automatic", "translation"],
            )
        else:
            st.write("Not creating a default word card")

        reset()
