# eesti-anki

The purpose of this project is to create Anki cards for Estonian language with Ukrainian translation.

## Installation

```
mkvirtualenv estonian
pip install -r requirements.txt
```

## Usage

- in Anki install [AnkiConnect](https://ankiweb.net/shared/info/2055492159) add-on that provides a local REST API to anki
- create a test deck in Anki in order not to ruin others (you can move cards later)
- run the main app using `streamlit run eesti.py`
